package mypackage;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlaceController {

    @RequestMapping ("/place")
    public String place(@RequestParam String mrt){
        return "MRT "+mrt;
    }
}
